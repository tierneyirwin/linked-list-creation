package testcases;
/**
 * Test cases for the LinkedList class testing removing methods, 
 * insert methods, size methods, and index methods.
 * 
 * @author Tierney Irwin
 * 
 * Due Date: 9 April 2016
 */
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import structures.LinkedList;

public class ListTests {

	private LinkedList<Integer> myLinkedList;
	@Before
	public void setUp() throws Exception 
	{
		myLinkedList = new LinkedList<Integer>();
	}
    /**
     * Test case to test if one can add 
     * and swiftly remove the same node from a list.
     * 
     */
	@Test
	public void testIfRemoveOnlyNode() 
	{
		myLinkedList.addFirst(1);
		myLinkedList.remove(1);
		assertTrue("Removed node", !myLinkedList.contains(1));
	}
	
	/**
     * Test case to test if one can 
     * remove something from an empty list.
     * 
     */
	@Test
	public void testIfRemoveEmpty()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		boolean valid = myLinkedList.remove(1);
		assertFalse("List is empty, nothing to remove", valid);
	}
	/**
     * Test case to test if one can remove a node from a list 
     * with a few nodes in it.
     * 
     */
	@Test
	public void testIfRemoveFromFew()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(1);
		myLinkedList.addFirst(2);
		myLinkedList.addFirst(3);
		myLinkedList.addFirst(4);
		myLinkedList.remove(3);
		assertFalse("Should not contain 3",myLinkedList.contains(3));
	}
	
	/**
     * Test case to test if one can remove a nonexistent 
     * node from a full list.
     * 
     */
	@Test
	public void testIfRemoveNonexistent()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(1);
		myLinkedList.addFirst(2);
		myLinkedList.addFirst(3);
		myLinkedList.addFirst(4);
		boolean valid = myLinkedList.remove(7);
		assertFalse("7 was never in list, nothing to remove",valid);
	}
	/**
     * Test case to test if one can remove something not there from
     * a list with only one element.
     * 
     */
	
	@Test
	public void testIfRemoveNoneFromOne()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(1);
		boolean valid = myLinkedList.remove(3);
		assertFalse("Never had 3, nothing to remove",valid);
	}

	/**
     * Test case to test if one can remove
     * the same node twice.
     * 
     */
	@Test
	public void testIfRemoveTwice()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(1);
		myLinkedList.addFirst(2);
		myLinkedList.addFirst(3);
		myLinkedList.addFirst(4);
		boolean valid = myLinkedList.remove(3);
		boolean second = myLinkedList.remove(3);
		assertFalse("Already removed",valid==second);
	}
	
	/**
     * Test case to test if one can remove one duplicate
     * and not both.
     * 
     */
	@Test
	public void testIfRemoveDuplicate()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(1);
		myLinkedList.addFirst(2);
		myLinkedList.addFirst(3);
		myLinkedList.addFirst(3);
		myLinkedList.remove(3);
		assertTrue("Should contain 3",myLinkedList.contains(3));
	}
	
	/**
     * Test case to test if one can remove a null
     * element from a list.
     * 
     */
	@Test
	public void testIfRemoveNull()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(1);
		boolean valid = myLinkedList.remove(null);
		assertFalse("Should not remove anything",valid);
	}
	
	/**
     * Test case to test if a list contains an added element.
     * 
     */
	@Test
	public void testIfContainsOnly()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(1);
		assertTrue("Should contain 1",myLinkedList.contains(1));
	}
	
	/**
     * Test case to test if a list contains multiple added elements.
     * 
     */
	@Test
	public void testIfContainsMultiple()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(1);
		myLinkedList.addFirst(12);
		myLinkedList.addFirst(4);
		myLinkedList.addLast(3);
		assertTrue("Should contain 1 and 4",myLinkedList.contains(1)&&myLinkedList.contains(4));
	}
	
	/**
     * Test case to test if a list contains something that isn't in the list.
     * 
     */
	@Test
	public void testIfNotContainsOnly()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(1);
		assertTrue("Should not contain 3",!myLinkedList.contains(3));
	}
	
	/**
     * Test case to test if a list contains something that isn't added to the list.
     * 
     */
	@Test
	public void testIfNotContainsMultiple()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(1);
		myLinkedList.addFirst(4);
		assertTrue("Should not contain 3",!myLinkedList.contains(3));
	}
	
	/**
     * Test case to test if a list contains nothing is true.
     * 
     */
	@Test
	public void testIfContainsNone()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		assertFalse("Should contain none",myLinkedList.contains(null));
	}
	
	/**
     * Test case to test if a list contains something in nothing.
     * 
     */
	@Test
	public void testIfNotContains()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		assertTrue("Should not contain 1",!myLinkedList.contains(1));
	}
	
	/**
     * Test case to test if a list contains a removed element.
     * 
     */
	@Test
	public void testIfContainsOneRemoved()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(1);
		myLinkedList.remove(1);
		assertTrue("Should not contain 1",!myLinkedList.contains(1));
	}
	
	/**
     * Test case to test if a list contains the removed elements.
     * 
     */
	@Test
	public void testIfContainsFewRemoved()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(1);
		myLinkedList.addFirst(4);
		myLinkedList.addLast(7);
		myLinkedList.remove(1);
		myLinkedList.remove(7);
		assertTrue("Should only contain 4", !myLinkedList.contains(1)&&!myLinkedList.contains(7));
	}
	
	/**
     * Test case to test if a list can insert before in an empty list.
     * 
     */
	@Test
	public void testIfInsertBeforeEmpty()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.insertBefore(1, null);
		assertTrue("Should have index of 0",myLinkedList.indexOf(1)==0);
	}
	
	/**
     * Test case to test if a list can insert before the only element in the list.
     * 
     */
	@Test
	public void testIfInsertBeforeOne()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(5);
		myLinkedList.insertBefore(1, 5);
		assertTrue("Should have index of 1",myLinkedList.indexOf(1)==1);
	}
	
	/**
     * Test case to test if a list can insert before an element in a full list.
     * 
     */
	@Test
	public void testIfInsertBeforeFew()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(2);
		myLinkedList.addFirst(6);
		myLinkedList.addLast(9);
		myLinkedList.insertBefore(1, 2);
		assertTrue("Should have index of 2",myLinkedList.indexOf(1)==2);
	}
	
	/**
     * Test case to test if a list can insert before the first element in a full list.
     * 
     */
	@Test
	public void testIfInsertBeforeFirstFew()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(3);
		myLinkedList.addFirst(9);
		myLinkedList.addFirst(90);
		myLinkedList.insertBefore(1, 90);
		assertTrue("Should have index of 1",myLinkedList.indexOf(1)==1);
	}

	/**
     * Test case to test if a list can insert before the last element in a full list.
     * 
     */
	@Test
	public void testIfInsertBeforeLastFew()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addLast(3);
		myLinkedList.addLast(9);
		myLinkedList.addFirst(90);
		myLinkedList.insertBefore(1, 9);
		assertTrue("Should have index of 3",myLinkedList.indexOf(1)==3);
	}
	
	/**
     * Test case to test if a list can insert before a null.
     * 
     */
	@Test
	public void testIfInsertBeforeNull()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.insertBefore(1, null);
		assertTrue("Should have index of 0",myLinkedList.indexOf(1)==0);
	}
	
	/**
     * Test case to test if index of an empty list is none.
     * 
     */
	@Test
	public void testIfIndexEmpty()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		assertTrue("Should have index of none",myLinkedList.indexOf(null)==-1);
	}
	
	/**
     * Test case to test if a index of one element list is 0.
     * 
     */
	@Test
	public void testIfIndexOfOne()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addLast(3);
		assertTrue("Should have index of 0",myLinkedList.indexOf(3)==0);
	}
	
	/**
     * Test case to test if index of random element is correct.
     * 
     */
	@Test
	public void testIfIndexOfFew()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addLast(3);
		myLinkedList.addLast(9);
		myLinkedList.addFirst(90);
		assertTrue("Should have index of 1",myLinkedList.indexOf(3)==1);
	}
	
	/**
     * Test case to test if corect index of removed element is none.
     * 
     */
	@Test
	public void testIfIndexOfRemoved()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addLast(3);
		myLinkedList.addLast(9);
		myLinkedList.addFirst(90);
		myLinkedList.remove(9);
		assertTrue("Should have index of none",myLinkedList.indexOf(9)==-1);
	}
	
	/**
     * Test case to test if index of nonexistent is none.
     * 
     */
	@Test
	public void testIfIndexOfNone()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addLast(3);
		myLinkedList.addLast(9);
		myLinkedList.addFirst(90);
		assertTrue("Should have index of none",myLinkedList.indexOf(5)==-1);
	}
	
	/**
     * Test case to test if able to remove first on empty list.
     * 
     */
	@Test
	public void testIfRemoveFirstEmpty()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		Integer valid = myLinkedList.removeFirst();
		assertTrue("Should not removed any",valid==null);
	}
	
	/**
     * Test case to test if able to remove first on one element list.
     * 
     */
	@Test
	public void testIfRemoveFirstOne()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(90);
		myLinkedList.removeFirst();
		assertTrue("Should be empty",!myLinkedList.contains(90));
	}
	
	/**
     * Test case to test if able to remove first on full list.
     * 
     */
	@Test
	public void testIfRemoveFirstFew()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addLast(3);
		myLinkedList.addLast(9);
		myLinkedList.addFirst(90);
		myLinkedList.removeFirst();
		myLinkedList.removeLast();
		assertTrue("Should not contain 90",!myLinkedList.contains(90));
		assertTrue("Should not contain 9",!myLinkedList.contains(9));
	}
	
	/**
     * Test case to test if able to remove last on empty.
     * 
     */
	@Test
	public void testIfRemoveLastEmpty()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		Integer valid = myLinkedList.removeLast();
		assertTrue("Should not have removed any",valid ==null);
	}
	
	/**
     * Test case to test if able to remove last on one element.
     * 
     */
	@Test
	public void testIfRemoveLastOne()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(90);
		myLinkedList.removeLast();
		assertTrue("Should be empty",!myLinkedList.contains(90));
	}
	
	/**
     * Test case to test if able to remove last on full list.
     * 
     */
	@Test
	public void testIfRemoveLastFew()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addLast(3);
		myLinkedList.addLast(9);
		myLinkedList.addFirst(90);
		myLinkedList.removeLast();
		myLinkedList.removeLast();
		assertTrue("Should not contain 9 or 3",!myLinkedList.contains(9)&&!myLinkedList.contains(3));
	}
	
	/**
     * Test case to test if size of empty is correct.
     * 
     */
	@Test
	public void testIfSizeEmpty()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		Integer size = myLinkedList.size();
		assertTrue("Should have size of 0",size==0);
	}
	
	/**
     * Test case to test if size of one element is correct.
     * 
     */
	@Test
	public void testIfSizeOne()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addFirst(8);
		Integer valid = myLinkedList.size();
		assertTrue("Should have size of 1",valid==1);
	}
	
	/**
     * Test case to test if size of full list is correct
     * 
     */
	@Test
	public void testIfSizeFew()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addLast(3);
		myLinkedList.addLast(9);
		myLinkedList.addFirst(90);
		Integer valid = myLinkedList.size();
		assertTrue("Should have size of 3",valid==3);
	}
	
	/**
     * Test case to test if size of removed one element list is correct.
     * 
     */
	@Test
	public void testIfSizeRemovedOne()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addLast(3);
		myLinkedList.addLast(9);
		Integer sizeFirst = myLinkedList.size();
		myLinkedList.removeFirst();
		Integer sizeLast = myLinkedList.size();
		assertTrue("Should have size different to original size",sizeFirst!=sizeLast);
	}
	
	/**
     * Test case to test if size of removed few elements is correct.
     * 
     */
	@Test
	public void testIfSizeRemovedFew()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addLast(3);
		myLinkedList.addLast(9);
		myLinkedList.addFirst(90);
		Integer sizeFirst = myLinkedList.size();
		myLinkedList.remove(3);
		myLinkedList.removeLast();
		Integer sizeLast = myLinkedList.size();
		assertTrue("Should different size than original",sizeFirst!=sizeLast);
	}
	
	/**
     * Test case to test if size of removed only element list is correct.
     * 
     */
	@Test
	public void testIfRemovedOnly()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addLast(3);
		Integer sizeFirst = myLinkedList.size();
		myLinkedList.removeFirst();
		Integer sizeLast = myLinkedList.size();
		assertTrue("Should have size of 0",sizeLast==0);
		assertTrue("Should not have same size",sizeLast!=sizeFirst);
	}
	
	/**
     * Test case to test if size of added elements is correct.
     * 
     */
	@Test
	public void testIfSizeAdded()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addLast(3);
		myLinkedList.addLast(9);
		Integer sizeFirst = myLinkedList.size();
		myLinkedList.addFirst(8);
		myLinkedList.addLast(9);
		Integer sizeLast = myLinkedList.size();
		assertTrue("Should have size of 2 plus the original size",sizeFirst!=sizeLast&&sizeLast == sizeFirst+2);
	}
	
	/**
     * Test case to test if size of one added list is correct.
     * 
     */
	@Test
	public void testIfSizeAddedOnly()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		Integer sizeFirst = myLinkedList.size();
		myLinkedList.addFirst(9);
		Integer sizeLast = myLinkedList.size();
		assertTrue("Should have size of 1 and different than original",sizeLast!=0&&sizeLast==1&&sizeFirst==0);
	}
	
	/**
     * Test case to test if set first of empty list is correct.
     * 
     */
	@Test
	public void testIfSetFirstEmpty()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.setFirst(9);
		assertTrue("Should have first equal to 9",myLinkedList.getFirst()==9);
	}
	
	/**
     * Test case to test if set first of one element list is correct.
     * 
     */
	@Test
	public void testIfSetFirstOne()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		Integer data = 3;
		Integer setData = 8;
		myLinkedList.addLast(data);
		myLinkedList.setFirst(setData);
		assertTrue("Should have first equal to 8",myLinkedList.getFirst()==setData);
	}
	
	/**
     * Test case to test if set first of full list is correct.
     * 
     */
	@Test
	public void testIfSetFirstFew()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addLast(3);
		myLinkedList.addLast(9);
		myLinkedList.addFirst(90);
		myLinkedList.setFirst(5);
		assertTrue("Should have first equal to 5",myLinkedList.getFirst() == 5);
	}
	
	/**
     * Test case to test if set first of one element multiple times is correct.
     * 
     */
	@Test
	public void testIfSetFirstMultiple()
	{
		LinkedList<Integer> myLinkedList = new LinkedList<Integer>();
		myLinkedList.addLast(3);
		myLinkedList.addLast(9);
		myLinkedList.addFirst(90);
		myLinkedList.setFirst(5);
		myLinkedList.setFirst(3);
		assertTrue("Should have first element equal to 3",myLinkedList.getFirst()==3);
	}
	

}
